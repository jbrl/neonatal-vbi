#!/bin/bash

if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` subject_list hemisphere [path_to_data]"
  exit 0
fi

if [ "$1" == "--help" ]; then
  echo "Usage: `basename $0` subject_list hemisphere [path_to_data]"
  exit 0
fi

if [ "$#" -lt 1 ]; then
  echo "Usage: `basename $0` subject_list hemisphere [path_to_data]"
  exit 0
fi

if [ "$#" -gt 2 ]; then
  DATAPATH=$3
else
  DATAPATH="."
fi

HEMI=$2

while read SUBJECT; do
  echo "Computing subject $SUBJECT..."
    ## DTI AND NODDI MAPS
    SUBJECTPATH="$DATAPATH/$SUBJECT"
    for MAP in FA MD L1 RD fintra_modulated ODItot_modulated; do
      DiffRes="`fslval /EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/coreg-diff-space/${SUBJECT}-mean_bzero_BRAIN.nii.gz pixdim1 | awk '{printf "%0.2f",$1}'`"
      NODDIMappingFWHM="`echo "$DiffRes * 2.5" | bc -l`"
      NODDIMappingSigma="`echo "$NODDIMappingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`"
      SmoothingFWHM="$DiffRes"
      SmoothingSigma="`echo "$SmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`"
      #/EBC/local/MRtrix3_stable/mrtrix3/bin/mrtransform ${SUBJECTPATH}/${SUBJECT}_ribbon_${HEMI}.nii.gz -replace /EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/derivatives/sub-session1/ses-${SUBJECT}/anat/sub-session1_ses-${SUBJECT}_T2w_restore_brain.nii.gz ${SUBJECTPATH}/${SUBJECT}_ribbon_${HEMI}_2.nii.gz -force
      #/EBC/home/mblesa/workbench/bin_linux64/wb_command -volume-to-surface-mapping $SUBJECTPATH/${SUBJECT}_${MAP}_str.nii.gz /EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/derivatives/sub-session1/ses-${SUBJECT}/anat/Native/sub-session1_ses-${SUBJECT}_${HEMI}_midthickness.surf.gii $SUBJECTPATH/${SUBJECT}_${HEMI}_${MAP}.native.shape.gii -myelin-style ${SUBJECTPATH}/${SUBJECT}_ribbon_${HEMI}_2.nii.gz /EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/derivatives/sub-session1/ses-${SUBJECT}/anat/Native/sub-session1_ses-${SUBJECT}_${HEMI}_corr_thickness.shape.gii "$NODDIMappingSigma"
      #/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-resample $SUBJECTPATH/${SUBJECT}_${HEMI}_${MAP}.native.shape.gii /EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/derivatives/sub-session1/ses-${SUBJECT}/anat/Native/sub-session1_ses-${SUBJECT}_${HEMI}_from-native_to-dhcpSym40_dens-32k_mode-sphere.reg40.surf.gii /EBC/preprocessedData/TEBC-BRIC2/dhcpSym_template/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_sphere.surf.gii ADAP_BARY_AREA $SUBJECTPATH/${SUBJECT}_${HEMI}_space-dhcpSym40_${MAP}.shape.gii -area-surfs /EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/derivatives/sub-session1/ses-${SUBJECT}/anat/Native/sub-session1_ses-${SUBJECT}_${HEMI}_midthickness.surf.gii /EBC/preprocessedData/TEBC-BRIC2/dhcpSym_template/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_midthickness.surf.gii
      #/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing /EBC/preprocessedData/TEBC-BRIC2/dhcpSym_template/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_midthickness.surf.gii $SUBJECTPATH/${SUBJECT}_${HEMI}_space-dhcpSym40_${MAP}.shape.gii "$SmoothingSigma" $SUBJECTPATH/${SUBJECT}_${HEMI}_space-dhcpSym40_${MAP}_smoothed.shape.gii -roi /EBC/home/mblesa/VB_INDEX/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii
    # Z-scoring metrics
    python zscore_gifti.py "$SUBJECTPATH/${SUBJECT}_${HEMI}_space-dhcpSym40_${MAP}_smoothed.shape.gii" "${SUBJECTPATH}/${SUBJECT}_${HEMI}_space-dhcpSym40_${MAP}_zscore.shape.gii"
    done
done <$1
