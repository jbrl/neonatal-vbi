import sys
import numpy as np
import nibabel as nib
from scipy.stats import zscore

def main():
    if len(sys.argv) <  4:
       print('Usage: impute_nans.py infile design medialwall_mask outname')
       sys.exit()
    maps = nib.load(sys.argv[1])
    design  = np.loadtxt(sys.argv[2])
    mask = nib.load(sys.argv[3])
    outname = sys.argv[4]
    term_idx = np.where(design[:,0]==0)[0]
    preterm_idx = np.where(design[:,0]==1)[0] 
    term_avg = np.nanmean(np.array([maps.darrays[idx].data for idx in term_idx]),axis=0)
    preterm_avg = np.nanmean(np.array([maps.darrays[idx].data for idx in preterm_idx]),axis=0)
    print('term',term_avg,'prem',preterm_avg)
    s_idx = 0
    for subject in maps.darrays:
        missing_idx = np.intersect1d(np.where(np.isnan(subject.data))[0],np.where(mask.darrays[0].data))
        if len(missing_idx) > 0:
            for m_idx in missing_idx:
                to_replace = term_avg[m_idx] if design[s_idx,0] == 0 else preterm_avg[m_idx]
                maps.darrays[s_idx].data[m_idx] = to_replace
        s_idx = s_idx + 1        
    nib.save(maps,outname)

if __name__ == '__main__':
    main()
