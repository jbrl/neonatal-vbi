# Neonatal VBI

Code to reproduce the analyses based on the Vogt-Bailey Index (VBI) applied to neonatal cortical data. The cose is provided as is, with no warranties, for documentation purposes only.


## How to cite
Galdi, P., Cabez, M. B., Farrugia, C., Vaher, K., Williams, L. Z. J., Sullivan, G., Stoye, D. Q., Quigley, A. J., Makropoulos, A., Thrippleton, M. J., Bastin, M. E., Richardson, H., Whalley, H., Edwards, A. D., Bajada, C. J., Robinson, E. C., & Boardman, J. P. (2024). Feature similarity gradients detect alterations in the neonatal cortex associated with preterm birth. Human Brain Mapping, 45(4), e26660. https://doi.org/10.1002/hbm.26660

