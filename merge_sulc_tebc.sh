#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "Usage: `basename $0` subject_list [path_to_data] [filename]"
  exit 0
fi

if [ "$#" -gt 1 ]; then
  DATAPATH=$2
else
  DATAPATH="."
fi

if [ "$#" -gt 2 ]; then
  FILENAME=$3
else
  FILENAME="all_sulc"
fi


maps_left=""
maps_right=""

#while IFS='-' read SUBJECT SESSION AGE; do
while read SUBJECT; do
  if [ ! -f "${DATAPATH}/${SUBJECT}/Structural_corrected/Surfaces_32K_noinit/sub-session1/ses-${SUBJECT}/dhcpSym_32k/sub-session1_ses-${SUBJECT}_left_space-dhcpSym40_sulc.shape.gii" ]
   then
    echo "${SUBJECT}"
    continue
  fi
  maps_left="${maps_left} -metric ${DATAPATH}/${SUBJECT}/Structural_corrected/Surfaces_32K_noinit/sub-session1/ses-${SUBJECT}/dhcpSym_32k/sub-session1_ses-${SUBJECT}_left_space-dhcpSym40_sulc.shape.gii"  
  maps_right="${maps_right} -metric ${DATAPATH}/${SUBJECT}/Structural_corrected/Surfaces_32K_noinit/sub-session1/ses-${SUBJECT}/dhcpSym_32k/sub-session1_ses-${SUBJECT}_right_space-dhcpSym40_sulc.shape.gii"  
done < $1 

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge "${FILENAME}_left.shape.gii" ${maps_left}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge "${FILENAME}_right.shape.gii" ${maps_right}

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-palette "${FILENAME}_left.shape.gii" MODE_AUTO_SCALE -palette-name ROY-BIG-BL
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-palette "${FILENAME}_right.shape.gii" MODE_AUTO_SCALE -palette-name ROY-BIG-BL
