import sys
import numpy as np
import nibabel as nib
from scipy.stats import zscore

def main():
    if len(sys.argv) <  3:
       print('Usage: zscore_gifti input_gifti output_gifti')
       sys.exit()
    filename = sys.argv[1]
    outname = sys.argv[2]
    gifti = nib.load(filename)
    for i in range(len(gifti.darrays)):
        arr = gifti.darrays[i].data
        mask = np.logical_and(arr!=0, arr!=np.nan)
        gifti.darrays[i].data[mask] = zscore(arr[mask])
    nib.save(gifti, outname)

if __name__ == '__main__':
    main()
