#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

L_mid_MERGELIST=""
R_mid_MERGELIST=""

L_sulc_MERGELIST=""
R_sulc_MERGELIST=""

L_thick_MERGELIST=""
R_thick_MERGELIST=""

while read p; do

  BASENAME=` basename ${p} | cut -d '-' -f 1 `

for hemi in right left; do

   echo ${BASENAME}

   /EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY ../${BASENAME}/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_thickness.shape.gii sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_thickness.shape.gii
   /EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY /EBC/preprocessedData/TEBC-BRIC2/${BASENAME}/Structural_corrected/Surfaces_32K_Schuh/sub-session1/ses-${BASENAME}/dhcpSym_32k/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_thickness.shape.gii sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_thickness.shape.gii

   /EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY ../${BASENAME}/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_sulc.shape.gii sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_sulc.shape.gii
   /EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY /EBC/preprocessedData/TEBC-BRIC2/${BASENAME}/Structural_corrected/Surfaces_32K_Schuh/sub-session1/ses-${BASENAME}/dhcpSym_32k/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_sulc.shape.gii sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_sulc.shape.gii

   /EBC/home/mblesa/workbench/bin_linux64/wb_command -surface-vertex-areas ../${BASENAME}/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_midthickness.surf.gii ${BASENAME}_${hemi}_midthick_va.shape.gii
   /EBC/home/mblesa/workbench/bin_linux64/wb_command -surface-vertex-areas /EBC/preprocessedData/TEBC-BRIC2/${BASENAME}/Structural_corrected/Surfaces_32K_Schuh/sub-session1/ses-${BASENAME}/dhcpSym_32k/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_midthickness.surf.gii ${BASENAME}_${hemi}_midthick_va.shape.gii

   done


   L_sulc_MERGELIST="${L_sulc_MERGELIST} -metric sub-session1_ses-${BASENAME}_left_space-dhcpSym40_sulc.shape.gii -metric sub-session1_ses-${BASENAME}_left_space-dhcpSym40_sulc.shape.gii"
R_sulc_MERGELIST="${R_sulc_MERGELIST} -metric sub-session1_ses-${BASENAME}_right_space-dhcpSym40_sulc.shape.gii -metric sub-session1_ses-${BASENAME}_right_space-dhcpSym40_sulc.shape.gii"

	   L_thick_MERGELIST="${L_thick_MERGELIST} -metric sub-session1_ses-${BASENAME}_left_space-dhcpSym40_thickness.shape.gii -metric sub-session1_ses-${BASENAME}_left_space-dhcpSym40_thickness.shape.gii"
	   R_thick_MERGELIST="${R_thick_MERGELIST} -metric sub-session1_ses-${BASENAME}_right_space-dhcpSym40_thickness.shape.gii -metric sub-session1_ses-${BASENAME}_right_space-dhcpSym40_thickness.shape.gii"

	   L_mid_MERGELIST="${L_mid_MERGELIST} -metric ${BASENAME}_left_midthick_va.shape.gii -metric ${BASENAME}_left_midthick_va.shape.gii"
	   R_mid_MERGELIST="${R_mid_MERGELIST} -metric ${BASENAME}_right_midthick_va.shape.gii -metric ${BASENAME}_right_midthick_va.shape.gii"

done < $1


/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge L_midthick_va.func.gii ${L_mid_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge R_midthick_va.func.gii ${R_mid_MERGELIST}

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce L_midthick_va.func.gii MEAN L_area.func.gii
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce R_midthick_va.func.gii MEAN R_area.func.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge L_sulc.shape.gii ${L_sulc_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge R_sulc.shape.gii ${R_sulc_MERGELIST}

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge L_thick.shape.gii ${L_thick_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge R_thick.shape.gii ${R_thick_MERGELIST}


