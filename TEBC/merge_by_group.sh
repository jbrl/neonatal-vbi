#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

VB_MERGELIST=""

hemi=$2

while read BASENAME; do
  VB_MERGELIST="${VB_MERGELIST} -metric ../VB/${BASENAME}_dmri-ribbon-mesh_${hemi}.unnorm.vbi-hybrid.shape.gii"
done < $1

if [[ $1 == *_preterm_* ]]; then
  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_dmri-ribbon-mesh_preterm_${hemi}.func.gii ${VB_MERGELIST}

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii VB_dmri-ribbon-mesh_preterm_${hemi}.func.gii 1.69864360057603808545 VB_smooth_ribbon-mesh_preterm_${hemi}.func.gii -roi ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce VB_smooth_ribbon-mesh_preterm_${hemi}.func.gii MEDIAN VB_preterm_${hemi}_median.shape.gii
fi


if [[ $1 == *_term_* ]]; then
  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_dmri-ribbon-mesh_term_${hemi}.func.gii ${VB_MERGELIST}

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii VB_dmri-ribbon-mesh_term_${hemi}.func.gii 1.69864360057603808545 VB_smooth_ribbon-mesh_term_${hemi}.func.gii -roi ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce VB_smooth_ribbon-mesh_term_${hemi}.func.gii MEDIAN VB_term_${hemi}_median.shape.gii
fi
