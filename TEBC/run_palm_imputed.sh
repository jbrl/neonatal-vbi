#!/bin/bash

Text2Vest design_$1.txt design_$1.mat
Text2Vest contrast_$1.txt contrast_$1.con

#/EBC/local/PALM/palm-alpha118/palm -i L_sulc.shape.gii -o palm_sulc_$1_L -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii L_area.func.gii -logp -n 1000
#/EBC/local/PALM/palm-alpha118/palm -i R_sulc.shape.gii -o palm_sulc_$1_R -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii R_area.func.gii -logp -n 1000
#/EBC/local/PALM/palm-alpha118/palm -i L_thick.shape.gii -o palm_thick_$1_L -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii L_area.func.gii -logp -n 1000
#/EBC/local/PALM/palm-alpha118/palm -i R_thick.shape.gii -o palm_thick_$1_R -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii R_area.func.gii -logp -n 1000
#if [[ $1 == *left* ]]; then
#  /EBC/local/PALM/palm-alpha118/palm -i VB_smooth_dmri-ribbon_left.func.gii -o palm_dmri-ribbon_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 10000 -zstat
#fi
#if [[ $1 == *right* ]]; then
#  /EBC/local/PALM/palm-alpha118/palm -i VB_smooth_dmri-ribbon_right.func.gii -o palm_dmri-ribbon_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 10000 -zstat
#fi
if [[ $1 == *left* ]]; then
  palm -i VB_imputed_smooth_ribbon-mesh_left.func.gii -o palm_dmri-ribbon-mesh_imputed_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s /EBC/home/mblesa/VB_INDEX/week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 10000 -zstat
fi
if [[ $1 == *right* ]]; then
  palm -i VB_imputed_smooth_ribbon-mesh_right.func.gii -o palm_dmri-ribbon-mesh_imputed_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s /EBC/home/mblesa/VB_INDEX/week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 10000 -zstat
fi
#if [[ $1 == *left* ]]; then
#  /EBC/local/PALM/palm-alpha118/palm -i VB_smooth_dmri_vbcut_left.func.gii -o palm_dmri_vbcut_$1_left -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 1000
#fi
#if [[ $1 == *right* ]]; then
#  /EBC/local/PALM/palm-alpha118/palm -i VB_smooth_dmri_vbcut_right.func.gii -o palm_dmri_vbcut_$1_right -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 1000
#fi
