#!/bin/bash

if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$1" == "--help" ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$#" -lt 2 ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$#" -gt 2 ]; then
  DATAPATH=$3
else
  DATAPATH="."
fi

HEMI=$2

VBPATH=/EBC/pgaldi/repos/py_vb_toolbox/vb_toolbox
#VBPATH=/EBC/pgaldi/vb_neonatal/py_vb_toolbox/vb_toolbox

#while IFS='-' read SUBJECT age v1 v2; do
while read SUBJECT; do
    SUBJECTPATH="$DATAPATH/$SUBJECT"
    #if [ ! -f ${SUBJECT}_dmri-ribbon-mesh_${HEMI}.unnorm.vbi-hybrid.shape.gii ]; then
      echo "Computing subject $SUBJECT, $HEMI hemisphere..."
      HM=${HEMI:0:1}
      HM=${HM^}

      # Merging maps
      fslmerge -t "${SUBJECTPATH}/${SUBJECT}_dmri-ribbon-mesh.nii.gz" "${SUBJECTPATH}/${SUBJECT}_FA_str.nii.gz" "${SUBJECTPATH}/${SUBJECT}_MD_str.nii.gz" "${SUBJECTPATH}/${SUBJECT}_L1_str.nii.gz" "${SUBJECTPATH}/${SUBJECT}_RD_str.nii.gz" "${SUBJECTPATH}/${SUBJECT}_fintra_modulated_str.nii.gz" "${SUBJECTPATH}/${SUBJECT}_ODItot_modulated_str.nii.gz"
      # Masking data
      fslmaths "${SUBJECTPATH}/${SUBJECT}_dmri-ribbon-mesh.nii.gz" -mas "/EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/derivatives/sub-session1/ses-${SUBJECT}/anat/sub-session1_ses-${SUBJECT}_brainmask_drawem.nii.gz" "${SUBJECTPATH}/${SUBJECT}_dmri-ribbon-mesh_masked.nii.gz"
      # Z-scoring metrics
      python zscore_nifti.py "${SUBJECTPATH}/${SUBJECT}_dmri-ribbon-mesh_masked.nii.gz" "${SUBJECTPATH}/${SUBJECT}_dmri-ribbon-mesh_zscore.nii.gz"
      # Computing VB-index
      ${VBPATH}/app.py -n unnorm --hybrid -vm  "${SUBJECTPATH}/${SUBJECT}_ribbon_${HEMI}.nii.gz" -m "/EBC/home/mblesa/VB_INDEX/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii" -s "/EBC/preprocessedData/TEBC-BRIC2/${SUBJECT}/Structural_corrected/Surfaces_32K_noinit/sub-session1/ses-${SUBJECT}/dhcpSym_32k/sub-session1_ses-${SUBJECT}_${HEMI}_space-dhcpSym40_midthickness.surf.gii" -d "${SUBJECTPATH}/${SUBJECT}_dmri-ribbon-mesh_zscore.nii.gz" -o "${SUBJECT}_dmri-ribbon-mesh_${HEMI}"
    #fi
done <$1
