#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "Usage: `basename $0` subject_list [path_to_data] [filename]"
  exit 0
fi

if [ "$#" -gt 1 ]; then
  DATAPATH=$2
else
  DATAPATH="."
fi

if [ "$#" -gt 2 ]; then
  FILENAME=$3
else
  FILENAME="all_sulc"
fi


maps_left=""
maps_right=""

while IFS=',' read SUBJECT SESSION AGE; do
  if [ ! -f "${DATAPATH}/${SUBJECT}/Surfaces_32K_Schuh_noinit/sub-${SUBJECT}/ses-${SESSION}/dhcpSym_32k/sub-${SUBJECT}_ses-${SESSION}_left_space-dhcpSym40_sulc.shape.gii" ]
   then
    echo "${SUBJECT}-${SESSION}"
    continue
  fi
  maps_left="${maps_left} -metric ${DATAPATH}/${SUBJECT}/Surfaces_32K_Schuh_noinit/sub-${SUBJECT}/ses-${SESSION}/dhcpSym_32k/sub-${SUBJECT}_ses-${SESSION}_left_space-dhcpSym40_sulc.shape.gii"  
  maps_right="${maps_right} -metric ${DATAPATH}/${SUBJECT}/Surfaces_32K_Schuh_noinit/sub-${SUBJECT}/ses-${SESSION}/dhcpSym_32k/sub-${SUBJECT}_ses-${SESSION}_right_space-dhcpSym40_sulc.shape.gii"  
done < $1 

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge "${FILENAME}_left.shape.gii" ${maps_left}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge "${FILENAME}_right.shape.gii" ${maps_right}

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-palette "${FILENAME}_left.shape.gii" MODE_AUTO_SCALE -palette-name ROY-BIG-BL
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-palette "${FILENAME}_right.shape.gii" MODE_AUTO_SCALE -palette-name ROY-BIG-BL
