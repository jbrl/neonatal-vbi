import sys
import numpy as np
import nibabel as nib
from scipy.stats import zscore

def main():
    if len(sys.argv) <  3:
       print('Usage: impute_nans.py infile medialwall_mask outname')
       sys.exit()
    maps = nib.load(sys.argv[1])
    mask = nib.load(sys.argv[2])
    outname = sys.argv[3]
    avg = np.nanmean(np.array([maps.darrays[idx].data for idx in range(len(maps.darrays))]),axis=0)
    s_idx = 0
    for subject in maps.darrays:
        missing_idx = np.intersect1d(np.where(np.isnan(subject.data))[0],np.where(mask.darrays[0].data))
        if len(missing_idx) > 0:
            for m_idx in missing_idx:
                to_replace = avg[m_idx]
                maps.darrays[s_idx].data[m_idx] = to_replace
        s_idx = s_idx + 1        
    nib.save(maps,outname)

if __name__ == '__main__':
    main()
