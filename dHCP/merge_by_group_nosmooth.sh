#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

VB_MERGELIST=""

hemi=$2

while IFS='-' read SUBJECT SESSION; do
  VB_MERGELIST="${VB_MERGELIST} -metric ${SUBJECT}_dmri_ribbon-mesh_${hemi}.unnorm.vbi-hybrid.shape.gii"
done < $1

if [[ $1 == *_preterm_* ]]; then
  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_dmri-ribbon-mesh_preterm_${hemi}.func.gii ${VB_MERGELIST}

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce VB_dmri-ribbon-mesh_preterm_${hemi}.func.gii MEDIAN VB_dhcp_preterm_${hemi}_median_nosmooth.shape.gii
fi


if [[ $1 == *_term_* ]]; then
  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_dmri-ribbon-mesh_term_${hemi}.func.gii ${VB_MERGELIST}

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce VB_dmri-ribbon-mesh_term_${hemi}.func.gii MEDIAN VB_dhcp_term_${hemi}_median_nosmooth.shape.gii
fi
