import sys
import numpy as np
import nibabel as nib
from scipy.stats import zscore

def main():
    if len(sys.argv) <  3:
       print('Usage: zscore_nifti input_gifti output_gifti')
       sys.exit()
    filename = sys.argv[1]
    outname = sys.argv[2]
    nifti = nib.load(filename)
    maps = np.array(nifti.dataobj)
    for i in range(maps.shape[-1]):
        imap = maps[:,:,:,i]
        mask = np.logical_and(imap!=0, imap!=np.nan)
        imap[mask] = zscore(imap[mask])
        maps[:,:,:,i] = imap
    nib.save(nib.Nifti1Image(maps, nifti.affine, header=nifti.header),outname)

if __name__ == '__main__':
    main()
