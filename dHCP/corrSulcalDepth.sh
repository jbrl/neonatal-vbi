#!/bin/bash

if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$1" == "--help" ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$#" -lt 2 ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$#" -gt 2 ]; then
  DATAPATH=$3
else
  DATAPATH="."
fi

HEMI=$2
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_no_intensity_correction'

while IFS='-' read SUBJECT SESSION; do
    SUBJECTPATH="$DATAPATH/$SUBJECT"

    echo "Computing subject $SUBJECT, $HEMI hemisphere..."
    HM=${HEMI:0:1}
    HM=${HM^}
  
    /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-regression ${SUBJECT}_dmri_ribbon-mesh_${HEMI}.unnorm.vbi-hybrid.shape.gii ${SUBJECT}_VBcorr_${HEMI}.unnorm.vbi-hybrid.shape.gii -roi "/EBC/home/mblesa/VB_INDEX/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii" -remove ${DHCP_PATH}/${SUBJECT}/Surfaces_32K_Schuh_noinit/sub-${SUBJECT}/ses-${SESSION}/dhcpSym_32k/sub-${SUBJECT}_ses-${SESSION}_${HEMI}_space-dhcpSym40_sulc.shape.gii
    /EBC/home/mblesa/workbench/bin_linux64/wb_command -set-map-name ${SUBJECT}_VBcorr_${HEMI}.unnorm.vbi-hybrid.shape.gii 1 ${SUBJECT}_corrVB

done <$1
