#!/bin/bash

if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$1" == "--help" ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$#" -lt 2 ]; then
  echo "Usage: `basename $0` subject_list hemi [path_to_data]"
  exit 0
fi

if [ "$#" -gt 2 ]; then
  DATAPATH=$3
else
  DATAPATH="."
fi

HEMI=$2

VBPATH=/EBC/pgaldi/repos/py_vb_toolbox/vb_toolbox
#VBPATH=/EBC/pgaldi/vb_neonatal/py_vb_toolbox/vb_toolbox

#while IFS='-' read SUBJECT age v1 v2; do
#while read SUBJECT; do
while IFS='-' read SUBJECT SESSION; do
    SUBJECTPATH="$DATAPATH/$SUBJECT"
    SURFPATH="$DATAPATH/$SUBJECT/Surfaces_32K_Schuh_noinit/sub-${SUBJECT}/ses-${SESSION}/dhcpSym_32k/"
    MAPSPATH="/EBC/home/mblesa/VB_INDEX/dHCP_corrected/$SUBJECT"
    echo "Computing subject $SUBJECT, $HEMI hemisphere..."
    HM=${HEMI:0:1}
    HM=${HM^}
    midthickness=$(find ${SURFPATH}/sub-${SUBJECT}_ses-${SESSION}_${HEMI}_space-dhcpSym40_midthickness.surf.gii)
    mask="/EBC/home/mblesa/VB_INDEX/week-40_hemi-${HEMI}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii"

    # Merging maps
    #fslmerge -t "${SUBJECT}_dmri.nii.gz" "${MAPSPATH}/${SUBJECT}_FA_str.nii.gz" "${MAPSPATH}/${SUBJECT}_MD_str.nii.gz" "${MAPSPATH}/${SUBJECT}_L1_str.nii.gz" "${MAPSPATH}/${SUBJECT}_RD_str.nii.gz" "${MAPSPATH}/${SUBJECT}_NDI_modulated_str.nii.gz" "${MAPSPATH}/${SUBJECT}_ODI_modulated_str.nii.gz"
    # Masking data
    #fslmaths "${SUBJECT}_dmri.nii.gz" -mas "${MAPSPATH}/sub-${SUBJECT}_ses-${SESSION}_desc-brain_mask.nii.gz" "${SUBJECT}_dmri_masked.nii.gz"
    # Z-scoring metrics
    #python zscore_nifti.py "${SUBJECT}_dmri_masked.nii.gz" "${SUBJECT}_dmri_zscore.nii.gz"
    # Computing VB-index
    ${VBPATH}/app.py -n unnorm --hybrid -vm  "${SUBJECTPATH}/${SUBJECT}_ribbon_${HEMI}.nii.gz" -m "${mask}" -s "${midthickness}" -d "${SUBJECT}_dmri_zscore.nii.gz" -o "${SUBJECT}_dmri_ribbon-mesh_${HEMI}"
done <$1
