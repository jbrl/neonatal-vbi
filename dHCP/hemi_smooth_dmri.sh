#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

mid_MERGELIST=""
VB_MERGELIST=""

hemi=$2

while IFS='-' read SUBJECT SESSION; do
  echo ${SUBJECT}

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -surface-vertex-areas ../${SUBJECT}/sub-${SUBJECT}_ses-${SESSION}_hemi-${hemi}_space-dhcpSym40_midthickness.surf.gii ${SUBJECT}_${hemi}_midthick_va.shape.gii
  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii ../VB/${SUBJECT}_dmri_${hemi}.unnorm.vbi-hybrid.shape.gii 1.69864360057603808545 ${SUBJECT}_dmri_${hemi}_smooth.unnorm.vbi-hybrid.shape.gii -roi ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY ${SUBJECT}_dmri_${hemi}_smooth.unnorm.vbi-hybrid.shape.gii ${SUBJECT}_dmri_${hemi}_smooth.unnorm.vbi-hybrid.shape.gii


  VB_MERGELIST="${VB_MERGELIST} -metric ${SUBJECT}_dmri_${hemi}_smooth.unnorm.vbi-hybrid.shape.gii"
  mid_MERGELIST="${mid_MERGELIST} -metric ${SUBJECT}_${hemi}_midthick_va.shape.gii"

done < $1

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_smooth_dmri_${hemi}.func.gii ${VB_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge ${hemi}_midthick_va_dmri.func.gii ${mid_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce ${hemi}_midthick_va_dmri.func.gii MEAN ${hemi}_area_dmri.func.gii


