#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

mid_MERGELIST=""
VB_MERGELIST=""
VB_smooth_MERGELIST=""

hemi=$2

while IFS='-' read SUBJECT SESSION; do
  SURFPATH="../$SUBJECT/Surfaces_32K_Schuh_noinit/sub-${SUBJECT}/ses-${SESSION}/dhcpSym_32k/"

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -surface-vertex-areas ${SURFPATH}/sub-${SUBJECT}_ses-${SESSION}_${hemi}_space-dhcpSym40_midthickness.surf.gii ${SUBJECT}_${hemi}_midthick_va.shape.gii

  VB_MERGELIST="${VB_MERGELIST} -metric ../VB/${SUBJECT}_dmri_ribbon-mesh_${hemi}.unnorm.vbi-hybrid.shape.gii"
  mid_MERGELIST="${mid_MERGELIST} -metric ${SUBJECT}_${hemi}_midthick_va.shape.gii"

done < $1

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_dmri_ribbon-mesh_${hemi}.func.gii ${VB_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge ${hemi}_midthick_va_dmri.func.gii ${mid_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce ${hemi}_midthick_va_dmri.func.gii MEAN ${hemi}_area_dmri.func.gii

python ../../impute_nans.py VB_dmri_ribbon-mesh_${hemi}.func.gii design_dhcp4_TvP_gas_sex_${hemi}.txt ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii VB_imputed_ribbon-mesh_${hemi}.func.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii VB_imputed_ribbon-mesh_${hemi}.func.gii 1.69864360057603808545 VB_imputed_smooth_ribbon-mesh_${hemi}.func.gii -roi ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY VB_imputed_smooth_ribbon-mesh_${hemi}.func.gii VB_imputed_smooth_ribbon-mesh_${hemi}.func.gii



