#!/bin/bash

Text2Vest design_$1.txt design_$1.mat
Text2Vest contrast_$1.txt contrast_$1.con

if [[ $1 == *left* ]]; then
  /EBC/local/PALM/palm-alpha118/palm -i VB_smooth_dmri_left.func.gii -o palm_dmri_$1_left -d design_$1.mat -t contrast_$1.con -T -tfce2D -s ../../week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 1000
fi
if [[ $1 == *right* ]]; then
  /EBC/local/PALM/palm-alpha118/palm -i VB_smooth_dmri_right.func.gii -o palm_dmri_$1_right -d design_$1.mat -t contrast_$1.con -T -tfce2D -s ../../week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 1000
fi
