#!/bin/bash

# ITERATE FOR ALL BASENAMES IN THE TXT FILE

TEBC_PATH='/EBC/preprocessedData/TEBC-BRIC2'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_no_intensity_correction'

hemi=$2

for MAP in corr_thickness curvature sulc; do
  MAP_MERGELIST=""
  while read BASENAME; do
    MAP_MERGELIST="${MAP_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}/Structural_corrected/Surfaces_32K_noinit/sub-session1/ses-${BASENAME}/dhcpSym_32k/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_${MAP}.shape.gii"
  done < $1

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge tebc3_${MAP}_${hemi}.func.gii $MAP_MERGELIST

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii tebc3_${MAP}_${hemi}.func.gii 1.69864360057603808545 tebc3_${MAP}_smooth_${hemi}.func.gii -roi ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY tebc3_${MAP}_smooth_${hemi}.func.gii tebc3_${MAP}_smooth_${hemi}.func.gii

done

# repeat for surface area

TEBC_PATH='/EBC/home/mblesa/VB_INDEX/TEBC_no_intensity_correction'
MAP="surface_area"
MAP_MERGELIST=""
while read BASENAME; do
  MAP_MERGELIST="${MAP_MERGELIST} -metric ${TEBC_PATH}/PALM/${BASENAME}_${hemi}_midthick_va.shape.gii"
done < $1

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge tebc3_${MAP}_${hemi}.func.gii $MAP_MERGELIST

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii tebc3_${MAP}_${hemi}.func.gii 1.69864360057603808545 tebc3_${MAP}_smooth_${hemi}.func.gii -roi ../../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY tebc3_${MAP}_smooth_${hemi}.func.gii tebc3_${MAP}_smooth_${hemi}.func.gii
