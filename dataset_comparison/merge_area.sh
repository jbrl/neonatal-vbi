#!/bin/bash

# ITERATE FOR ALL BASENAMES IN THE TXT FILE

TEBC_PATH='/EBC/home/mblesa/VB_INDEX/TEBC_no_intensity_correction'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_no_intensity_correction'

hemi=$3

MAP_MERGELIST=""
while read BASENAME; do
  MAP_MERGELIST="${MAP_MERGELIST} -metric ${TEBC_PATH}/PALM/${BASENAME}_${hemi}_midthick_va.shape.gii"
done < $1

while IFS='-' read BASENAME SESSION; do
  MAP_MERGELIST="${MAP_MERGELIST} -metric ${DHCP_PATH}/PALM/${BASENAME}_${hemi}_midthick_va.shape.gii"
done < $2

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge comparison_surface_area_${hemi}.func.gii $MAP_MERGELIST

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii comparison_surface_area_${hemi}.func.gii 1.69864360057603808545 comparison_surface_area_smooth_${hemi}.func.gii -roi ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY comparison_surface_area_smooth_${hemi}.func.gii comparison_surface_area_smooth_${hemi}.func.gii


