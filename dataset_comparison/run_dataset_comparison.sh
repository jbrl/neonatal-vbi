#!/bin/bash

Text2Vest design_$1.txt design_$1.mat
Text2Vest contrast_$1.txt contrast_$1.con

MAP=$2

if [[ $1 == *left* ]]; then
  PALM/palm -i comparison_${MAP}_smooth_left.func.gii -o palm_comparison_${MAP}_left -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 10000 -zstat
fi
if [[ $1 == *right* ]]; then
  PALM/palm -i comparison_${MAP}_smooth_right.func.gii -o palm_comparison_${MAP}_right -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 10000 -zstat -precision double -seed 42
fi
