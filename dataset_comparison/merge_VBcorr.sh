#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

VB_MERGELIST=""
TEBC_PATH='/EBC/home/mblesa/VB_INDEX/TEBC_no_intensity_correction/VB'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_no_intensity_correction/VB'

hemi=$3

while read BASENAME; do
  VB_MERGELIST="${VB_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}_VBcorr_${hemi}.unnorm.vbi-hybrid.shape.gii"
done < $1

while IFS='-' read BASENAME SESSION; do
  VB_MERGELIST="${VB_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}_VBcorr_${hemi}.unnorm.vbi-hybrid.shape.gii"
done < $2

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VBcorr_dataset_comparison_${hemi}.func.gii ${VB_MERGELIST}

python ../impute_nans.py VBcorr_dataset_comparison_${hemi}.func.gii design_dHCPvTEBC2_preterm_gas_sex_${hemi}.txt ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii VBcorr_dataset_comparison_imputed_${hemi}.func.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii VBcorr_dataset_comparison_imputed_${hemi}.func.gii 1.69864360057603808545 VBcorr_smooth_dataset_comparison_${hemi}.func.gii -roi ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY VBcorr_smooth_dataset_comparison_${hemi}.func.gii VBcorr_smooth_dataset_comparison_${hemi}.func.gii



