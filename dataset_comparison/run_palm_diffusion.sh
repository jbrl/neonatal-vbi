#!/bin/bash

Text2Vest design_$1.txt design_$1.mat
Text2Vest contrast_$1.txt contrast_$1.con

INPUTS_L=""
INPUTS_R=""

#for MAP in FA MD L1 RD NDI_modulated ODI_modulated; do
#  INPUTS_L="${INPUTS_L} -i diffusion_comparison_imputed_${MAP}_left.func.gii"
#  INPUTS_R="${INPUTS_R} -i diffusion_comparison_imputed_${MAP}_right.func.gii"
#done
#
#if [[ $1 == *left* ]]; then
#  palm $INPUTS_L -o palm_diffusion_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s ../week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 10000 -zstat -precision double
#fi
#if [[ $1 == *right* ]]; then
#  palm $INPUTS_R -o palm_diffusion_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s ../week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 10000 -zstat -precision double
#fi

for MAP in FA MD L1 RD NDI_modulated ODI_modulated; do
  INPUTS_L="-i diffusion_comparison_imputed_${MAP}_left.func.gii"
  INPUTS_R="-i diffusion_comparison_imputed_${MAP}_right.func.gii"

  if [[ $1 == *left* ]]; then
    palm $INPUTS_L -o palm10k_${MAP}_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s ../week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 10000 -zstat -precision double
  fi
  if [[ $1 == *right* ]]; then
    palm $INPUTS_R -o palm10k_${MAP}_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s ../week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 10000 -zstat -precision double
  fi
done
