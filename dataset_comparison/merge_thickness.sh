#!/bin/bash

# ITERATE FOR ALL BASENAMES IN THE TXT FILE

#mid_MERGELIST=""
TEBC_PATH='/EBC/preprocessedData/TEBC-BRIC2'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_no_intensity_correction'

hemi=$3

for MAP in corr_thickness; do
  MAP_MERGELIST=""
  while read BASENAME; do
    MAP_MERGELIST="${MAP_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}/Structural_corrected/Surfaces_32K_noinit/sub-session1/ses-${BASENAME}/dhcpSym_32k/sub-session1_ses-${BASENAME}_${hemi}_space-dhcpSym40_${MAP}.shape.gii"
  done < $1

  while IFS='-' read BASENAME SESSION; do
    MAP_MERGELIST="${MAP_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}//Surfaces_32K_Schuh_noinit/sub-${BASENAME}/ses-${SESSION}/dhcpSym_32k/sub-${BASENAME}_ses-${SESSION}_${hemi}_space-dhcpSym40_${MAP}.shape.gii"
  done < $2

  /EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge thickness_comparison_${MAP}_${hemi}.func.gii $MAP_MERGELIST

done
#/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge ${hemi}_midthick_va_dmri.func.gii ${mid_MERGELIST}
#/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce ${hemi}_midthick_va_dmri.func.gii MEAN ${hemi}_area_dmri.func.gii
