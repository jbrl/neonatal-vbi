#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

mid_MERGELIST=""
#VB_MERGELIST=""
TEBC_PATH='/EBC/home/mblesa/VB_INDEX/TEBC_no_intensity_correction/PALM'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_no_intensity_correction/PALM'

hemi=$3

while read BASENAME; do
  #VB_MERGELIST="${VB_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}_dmri-ribbon-mesh_imputed_${hemi}.unnorm.vbi-hybrid.shape.gii"
  mid_MERGELIST="${mid_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}_${hemi}_midthick_va.shape.gii"
done < $1

while IFS='-' read BASENAME SESSION; do
  #VB_MERGELIST="${VB_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}_dmri_ribbon-mesh_imputed_${hemi}_smooth.unnorm.vbi-hybrid.shape.gii"
  mid_MERGELIST="${mid_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}_${hemi}_midthick_va.shape.gii"
done < $2

#/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_imputed_ribbon-mesh_${hemi}.func.gii ${VB_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_imputed_ribbon-mesh_${hemi}.func.gii -metric ${TEBC_PATH}/VB_imputed_ribbon-mesh_${hemi}.func.gii -metric ${DHCP_PATH}/VB_imputed_ribbon-mesh_${hemi}.func.gii
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge ${hemi}_midthick_va_dmri.func.gii ${mid_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce ${hemi}_midthick_va_dmri.func.gii MEAN ${hemi}_area_dmri.func.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_midthickness.surf.gii VB_imputed_ribbon-mesh_${hemi}.func.gii 1.69864360057603808545 VB_imputed_smooth_ribbon-mesh_${hemi}.func.gii -roi ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY VB_imputed_smooth_ribbon-mesh_${hemi}.func.gii VB_imputed_smooth_ribbon-mesh_${hemi}.func.gii



