#!/bin/bash

Text2Vest design_$1.txt design_$1.mat
Text2Vest contrast_$1.txt contrast_$1.con

if [[ $1 == *left* ]]; then
  PALM/palm -i VBcorr_smooth_dataset_comparison_left.func.gii -o palm_vbcorr_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii -logp -n 10000 -zstat -evperdat comparison_corr_thickness_smooth_left.func.gii 5 -evperdat comparison_surface_area_smooth_left.func.gii 6 -precision double
fi
if [[ $1 == *right* ]]; then
  PALM/palm -i VBcorr_smooth_dataset_comparison_right.func.gii -o palm_vbcorr_$1 -d design_$1.mat -t contrast_$1.con -T -tfce2D -s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii -logp -n 10000 -zstat -evperdat comparison_corr_thickness_smooth_right.func.gii 5 -evperdat comparison_surface_area_smooth_right.func.gii 6 -precision double
fi
