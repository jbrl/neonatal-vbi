#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

mid_MERGELIST=""
VB_MERGELIST=""
TEBC_PATH='/EBC/home/mblesa/VB_INDEX/TEBC_corrected_reg/PALM'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dHCP_corrected/PALM'

hemi=$3

while read p; do
  BASENAME=` basename ${p} | cut -d '-' -f 1 `

  VB_MERGELIST="${VB_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}_dmri_${hemi}_smooth.unnorm.vbi_hybrid.shape.gii"
  mid_MERGELIST="${mid_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}_${hemi}_midthick_va.shape.gii"

done < $1

while IFS='-' read BASENAME SESSION; do

  VB_MERGELIST="${VB_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}_dmri_${hemi}_smooth.unnorm.vbi-hybrid.shape.gii"
  mid_MERGELIST="${mid_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}_${hemi}_midthick_va.shape.gii"

done < $2

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge VB_smooth_dmri_${hemi}.func.gii ${VB_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge ${hemi}_midthick_va_dmri.func.gii ${mid_MERGELIST}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce ${hemi}_midthick_va_dmri.func.gii MEAN ${hemi}_area_dmri.func.gii


