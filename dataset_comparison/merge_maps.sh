#!/bin/bash

# ITERATE FOR ALL BASENAMES IN THE TXT FILE

#mid_MERGELIST=""
TEBC_PATH='/EBC/home/mblesa/VB_INDEX/dataset_comparison'
DHCP_PATH='/EBC/home/mblesa/VB_INDEX/dataset_comparison'

hemi=$3

for MAP in FA MD L1 RD fintra_modulated ODItot_modulated; do
  MAP_MERGELIST=""
  while read BASENAME; do
    MAP_MERGELIST="${MAP_MERGELIST} -metric ${TEBC_PATH}/${BASENAME}_${hemi}_space-dhcpSym40_${MAP}_zscore.shape.gii"
  done < $1

  while IFS='-' read BASENAME SESSION; do
    if [[ "$MAP" == 'ODItot_modulated' ]]; then MAP='ODI_modulated'; fi
    if [[ "$MAP" == 'fintra_modulated' ]]; then MAP='NDI_modulated'; fi
    MAP_MERGELIST="${MAP_MERGELIST} -metric ${DHCP_PATH}/${BASENAME}_${hemi}_space-dhcpSym40_${MAP}_zscore.shape.gii"
  done < $2

  #/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge diffusion_comparison_${MAP}_${hemi}.func.gii $MAP_MERGELIST
  python ../impute_nans.py diffusion_comparison_${MAP}_${hemi}.func.gii design_dHCPvTEBC2_preterm_gas_sex_${hemi}.txt ../week-40_hemi-${hemi}_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii diffusion_comparison_imputed_${MAP}_${hemi}.func.gii

done
#/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge ${hemi}_midthick_va_dmri.func.gii ${mid_MERGELIST}
#/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-reduce ${hemi}_midthick_va_dmri.func.gii MEAN ${hemi}_area_dmri.func.gii
