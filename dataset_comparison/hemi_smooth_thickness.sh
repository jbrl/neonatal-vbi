#!/bin/bash

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii thickness_comparison_corr_thickness_left.func.gii 1.69864360057603808545 thickness_comparison_corr_thickness_smooth_left.func.gii -roi ../week-40_hemi-left_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY thickness_comparison_corr_thickness_smooth_left.func.gii thickness_comparison_corr_thickness_smooth_left.func.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-smoothing ../week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii thickness_comparison_corr_thickness_right.func.gii 1.69864360057603808545 thickness_comparison_corr_thickness_smooth_right.func.gii -roi ../week-40_hemi-right_space-dhcpSym_dens-32k_desc-medialwallsymm_mask.shape.gii

/EBC/home/mblesa/workbench/bin_linux64/wb_command -gifti-convert BASE64_BINARY thickness_comparison_corr_thickness_smooth_right.func.gii thickness_comparison_corr_thickness_smooth_right.func.gii



