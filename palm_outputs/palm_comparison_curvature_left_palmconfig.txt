# Configuration file for PALM.
# Version (github, running in MATLAB 9.4.0.813654 (R2018a).
# 11-Apr-2023 01:31:14

-i comparison_curvature_smooth_left.func.gii
-o palm_comparison_curvature_left
-d design_dHCPvTEBC2_preterm_gas_sex_left.mat
-t contrast_dHCPvTEBC2_preterm_gas_sex_left.con
-T
-tfce2D
-s week-40_hemi-left_space-dhcpSym_dens-32k_midthickness.surf.gii left_area_dmri.func.gii
-logp
-n 10000
-zstat
