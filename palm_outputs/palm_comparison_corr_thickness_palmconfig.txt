# Configuration file for PALM.
# Version (github, running in MATLAB 9.4.0.813654 (R2018a).
# 08-Apr-2023 03:49:06

-i comparison_corr_thickness_smooth_right.func.gii
-o palm_comparison_corr_thickness
-d design_dHCPvTEBC2_preterm_gas_sex_right.mat
-t contrast_dHCPvTEBC2_preterm_gas_sex_right.con
-T
-tfce2D
-s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii
-logp
-n 10000
-zstat
