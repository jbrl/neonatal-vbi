# Configuration file for PALM.
# Version (github, running in MATLAB 9.4.0.813654 (R2018a).
# 31-Jul-2023 21:41:00

-i VBcorr_smooth_dataset_comparison_right.func.gii
-o palm_vbcorr_dHCPvTEBC2_vbcorr_thick_area_right
-d design_dHCPvTEBC2_vbcorr_thick_area_right.mat
-t contrast_dHCPvTEBC2_vbcorr_thick_area_right.con
-T
-tfce2D
-s week-40_hemi-right_space-dhcpSym_dens-32k_midthickness.surf.gii right_area_dmri.func.gii
-logp
-n 10000
-zstat
-evperdat comparison_corr_thickness_smooth_right.func.gii 5
-evperdat comparison_surface_area_smooth_right.func.gii 6
-precision double
