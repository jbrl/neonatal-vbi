#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "Usage: `basename $0` subject_list [path_to_data] [filename]"
  exit 0
fi

if [ "$#" -gt 1 ]; then
  DATAPATH=$2
else
  DATAPATH="."
fi

if [ "$#" -gt 2 ]; then
  FILENAME=$3
else
  FILENAME="all_vb"
fi


maps_left=""
maps_right=""

while IFS='-' read SUBJECT SESSION; do
  if [ -f "${DATAPATH}/${SUBJECT}_${FILENAME}_right.unnorm.vbi-hybrid.shape.gii" ]
   then
    maps_right="${maps_right} -metric ${DATAPATH}/${SUBJECT}_${FILENAME}_right.unnorm.vbi-hybrid.shape.gii"
  fi
  if [ -f "${DATAPATH}/${SUBJECT}_${FILENAME}_left.unnorm.vbi-hybrid.shape.gii" ]
   then
    maps_left="${maps_left} -metric ${DATAPATH}/${SUBJECT}_${FILENAME}_left.unnorm.vbi-hybrid.shape.gii"
  fi
done < $1 

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge "dhcp_${FILENAME}_left.shape.gii" ${maps_left}
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-merge "dhcp_${FILENAME}_right.shape.gii" ${maps_right}

/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-palette "${FILENAME}_left.shape.gii" MODE_AUTO_SCALE -palette-name ROY-BIG-BL
/EBC/home/mblesa/workbench/bin_linux64/wb_command -metric-palette "${FILENAME}_right.shape.gii" MODE_AUTO_SCALE -palette-name ROY-BIG-BL
